import 'dart:io';

import 'package:device_info/device_info.dart';

class HelperMethods {
  Future<String> getId() async {
    var deviceInfo = DeviceInfoPlugin();
    if (Platform.isIOS) {
      var iosInfo = await deviceInfo.iosInfo;
      return iosInfo.identifierForVendor;
    } else {
      var androidInfo = await deviceInfo.androidInfo;
      return androidInfo.androidId;
    }
  }
}
