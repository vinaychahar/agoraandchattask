import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:agora_voice_call_task/helper_files/constants.dart';
import 'package:flutter/material.dart';

class VoiceCallScreen extends StatefulWidget {
  final String callingOnChannel;

  VoiceCallScreen(this.callingOnChannel);

  @override
  _VoiceCallScreenState createState() => _VoiceCallScreenState();
}

class _VoiceCallScreenState extends State<VoiceCallScreen> {
  RtcEngine agoraClient;
  bool isMuted = false;

  @override
  void initState() {
    super.initState();

    _setupAgoraEngine();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Container(
          color: Colors.white,
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("Call in Progress"),
                SizedBox(height: 60),
                Container(
                  height: 70,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        width: 60,
                        height: 60,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(30),
                            border: Border.all(width: 1, color: Colors.grey)),
                        child: IconButton(
                            color: Colors.red,
                            icon: Icon(Icons.call_end),
                            onPressed: _disconnectCall),
                      ),
                      SizedBox(width: 30),
                      Container(
                        width: 60,
                        height: 60,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(30),
                            border: Border.all(width: 1, color: Colors.grey)),
                        child: IconButton(
                            color: isMuted ? Colors.grey : Colors.green,
                            icon: Icon(Icons.mic),
                            onPressed: _muteUnMute),
                      ),
                    ],
                  ),
                )
              ],
            ),
          )),
    );
  }

  //Agora Methods
  _setupAgoraEngine() async {
    RtcEngine.create(Constants.agoraAppID).then((agoraClient1) {
      this.agoraClient = agoraClient1;
      print("Agora: $agoraClient");
      agoraClient.enableAudio();
      _startAgoraCall();
      _addEventHandlersForCall();
    });
  }

  _startAgoraCall() {
    String token = Constants.token1;
    agoraClient.joinChannel(token, widget.callingOnChannel, null, 0);
  }

  _addEventHandlersForCall() {
    agoraClient.setEventHandler(RtcEngineEventHandler(
      error: (errorCode) {
        print("Error Code $errorCode");
      },
      joinChannelSuccess: (channel, uid, elapsed) {
        print("Channel Joined $channel, $uid, $elapsed");
      },
      leaveChannel: (stats) {
        print("Leaving Channel $stats");
      },
      userJoined: (uid, elapsed) {
        print("User Joined $uid, $elapsed");
      },
      userOffline: (uid, reason) {
        print("User Offline $uid, $reason");
      },
    ));
  }

  _muteUnMute() {
    setState(() {
      this.isMuted = !this.isMuted;
    });
    agoraClient.muteLocalAudioStream(this.isMuted);
  }

  _disconnectCall() {
    agoraClient.leaveChannel();
    Navigator.of(context).pop();
  }
}
