import 'package:agora_voice_call_task/chat_screens/conversation_screen.dart';
import 'package:agora_voice_call_task/helper_files/constants.dart';
import 'package:agora_voice_call_task/screens/screen_voice_call.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';

class LandingScreen extends StatefulWidget {
  @override
  _LandingScreenState createState() => _LandingScreenState();
}

class _LandingScreenState extends State<LandingScreen> {
  TextEditingController _controllerChannelName =
      TextEditingController(text: Constants.channelName);
  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Task"),
      ),
      body: Column(
        children: [
          Container(
              margin: EdgeInsets.all(20),
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                  border: Border.all(width: 1, color: Colors.grey)),
              child: Column(
                children: [
                  Text(
                      "Enter Channel Name\n(use the following channel name for test purpose)"),
                  _getTextField(),
                  _getButtonToStartCall()
                ],
              )),
          SizedBox(
            height: 30,
          ),
          Container(
              decoration: BoxDecoration(
                  border: Border.all(width: 1, color: Colors.grey)),
              height: 60,
              width: MediaQuery.of(context).size.width - 40,
              margin: EdgeInsets.all(20),
              padding: EdgeInsets.zero,
              child: FlatButton(
                  color: Colors.blue,
                  onPressed: _openChatScreen,
                  child: Text("Chat Task",
                      style: TextStyle(fontSize: 20, color: Colors.white))))
        ],
      ),
    );
  }

  Widget _getTextField() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(30, 20, 30, 0),
      child: Card(
        child: Container(
          height: 50,
          child: CupertinoTextField(
            onChanged: (value) {
              // setState(() {
              // });
            },
            controller: _controllerChannelName,
            textAlign: TextAlign.center,
            keyboardType: TextInputType.text,
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: (_controllerChannelName.text.length == 0) ? 17 : 30,
                color: Colors.black),
            placeholder: "Channel Name",
          ),
        ),
      ),
    );
  }

  _getButtonToStartCall() {
    return Container(
      width: 200,
      height: 70,
      padding: EdgeInsets.only(top: 10, bottom: 10),
      child: FlatButton(
          color: Colors.cyan,
          child: Text("Start Voice Call"),
          onPressed: _btnStartCallPressed),
    );
  }

  //Button Actions
  _btnStartCallPressed() async {
    setState(() {
      isLoading = true;
    });

    if (await Permission.microphone.request().isGranted) {
      Navigator.of(context).push(MaterialPageRoute(builder: (context) {
        return VoiceCallScreen(_controllerChannelName.text);
      }));
    } else {
      //Permission not granted

    }
  }

  _openChatScreen() {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) {
      return ConversationScreen();
    }));
  }
}
