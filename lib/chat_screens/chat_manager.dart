import 'dart:convert';
import 'package:agora_voice_call_task/chat_screens/message.dart';
import 'package:agora_voice_call_task/helper_files/constants.dart';
import 'package:agora_voice_call_task/helper_files/helper.dart';
import 'package:agora_voice_call_task/helper_files/shared_preferences_manager.dart';
import 'package:web_socket_channel/io.dart';

class ChatManager {
  final channel = IOWebSocketChannel.connect(Constants.socketAddress);
  static ChatManager _instance = ChatManager._internal();

  static getInstance() => _instance;
  String userID;

  ChatManager._internal() {
    setUserId();
  }

  sendMessage(ChatMessage message) {
    channel.sink.add(json.encode(message.toJson()));
  }

  setUserId() async {
    userID = PreferencesManager.getPref(Constants.USER_ID_PREF_KEY) ?? "";
    if (userID.length == 0) {
      userID = await HelperMethods().getId();
      PreferencesManager.savePref(Constants.USER_ID_PREF_KEY, userID);
    }
    return userID;
  }

  getUserId() {
    userID = PreferencesManager.getPref(Constants.USER_ID_PREF_KEY) ?? "";
    return userID;
  }

//Socket Methods
  getSocketChannelStream() {
    return channel.stream;
  }

  closeConnection() {
    channel.sink.close();
  }
}
