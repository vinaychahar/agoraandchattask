import 'dart:convert';

import 'package:agora_voice_call_task/chat_screens/chat_manager.dart';

//Conversion Helper Methods
ChatMessage chatMessageFromJson(String str) =>
    ChatMessage.fromJson(json.decode(str));

String chatMessageToJson(ChatMessage data) => json.encode(data.toJson());

//Class for creating a Chat Message instance. Used for handling received and sent message
class ChatMessage {
  ChatMessage({this.senderID, this.message, this.createdAt});

  final String senderID;
  final String message;
  final String createdAt;

  factory ChatMessage.fromJson(Map<String, dynamic> json) => ChatMessage(
      senderID: json["sender_id"],
      message: json["message"],
      createdAt: json["created_at"]);

  Map<String, dynamic> toJson() =>
      {"sender_id": senderID, "message": message, "created_at": createdAt};

  //Helper method
  bool isMyMessage() {
    final ChatManager _chatManager = ChatManager.getInstance();
    return (this.senderID == _chatManager.getUserId());
  }
}
