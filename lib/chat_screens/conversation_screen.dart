import 'dart:async';
import 'dart:convert';
import 'package:agora_voice_call_task/chat_screens/chat_manager.dart';
import 'package:agora_voice_call_task/chat_screens/message.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class ConversationScreen extends StatelessWidget {
  final controllerMessage = TextEditingController();

  final ScrollController _scrollController = new ScrollController();
  final ChatManager _chatManager = ChatManager.getInstance();
  final List<ChatMessage> _chatMessages = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Chat"),
      ),
      body: SafeArea(
        child: StreamBuilder(
          stream: _chatManager.getSocketChannelStream(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              _chatMessages
                  .add(ChatMessage.fromJson(json.decode(snapshot.data)));
              if (_chatMessages.length > 1) {
                Future.delayed(Duration(milliseconds: 50), () {
                  _scrollToBottom();
                });
              }
            }

            return Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(height: 15.0),
                Expanded(
                  child: (_chatMessages.length == 0)
                      ? _getNoMessagePlaceholderWidget()
                      : _getConversationListView(context),
                ),
                _getChatInputTextFieldAndSendButton(context),
              ],
            );
          },
        ),
      ),
    );
  }

  Container _getChatInputTextFieldAndSendButton(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Container(
      width: width,
      height: 70.0,
      padding: EdgeInsets.all(10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: Container(
              width: width - 70.0,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10.0),
                border: Border.all(width: 1, color: Colors.grey),
                color: Colors.white,
              ),
              child: TextField(
                controller: controllerMessage,
                style: TextStyle(
                  fontSize: 13.0,
                  color: Colors.white,
                ),
                decoration: InputDecoration(
                  hintText: "Type your message here",
                  hintStyle: TextStyle(
                    fontSize: 13.0,
                    color: Colors.grey,
                  ),
                  contentPadding:
                      (Localizations.localeOf(context).languageCode == 'ar')
                          ? EdgeInsets.only(right: 10.0)
                          : EdgeInsets.only(left: 10.0),
                  border: InputBorder.none,
                ),
              ),
            ),
          ),
          SizedBox(width: 5.0),
          SizedBox(
            width: 70.0,
            height: 60.0,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(7),
              child: RaisedButton(
                padding: EdgeInsets.all(2.0),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(0.0),
                ),
                onPressed: _sendMessageTapped,
                color: Colors.blue,
                child: Icon(
                  Icons.send,
                  color: Colors.white,
                  size: 28.0,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _sendMessageTapped() {
    if (controllerMessage.text.trim().length > 0) {
      ChatMessage chatMessage = ChatMessage.fromJson(
        {
          "sender_id": _chatManager.getUserId(),
          "message": controllerMessage.text.trim(),
          "created_at": DateFormat("dd/MM/yyyy hh:mm a").format(DateTime.now())
        },
      );
      _chatManager.sendMessage(chatMessage);
      controllerMessage.text = ''; //Empty textfield after sending message
    }
  }

  ListView _getConversationListView(BuildContext context) {
    return ListView.builder(
      controller: _scrollController,
      itemCount: _chatMessages.length,
      physics: BouncingScrollPhysics(),
      itemBuilder: (context, index) {
        final item = _chatMessages[index];
        bool isMyMessage = !item.isMyMessage();

        return Container(
          width: MediaQuery.of(context).size.width,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment:
                isMyMessage ? CrossAxisAlignment.start : CrossAxisAlignment.end,
            children: <Widget>[
              Wrap(
                children: <Widget>[
                  Padding(
                    padding: isMyMessage
                        ? EdgeInsets.only(left: 20.0)
                        : EdgeInsets.only(right: 20.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: isMyMessage
                          ? CrossAxisAlignment.start
                          : CrossAxisAlignment.end,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.all(10.0),
                          margin: EdgeInsets.only(left: 10.0, right: 10.0),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5.0),
                            color:
                                isMyMessage ? Colors.blueAccent : Colors.green,
                          ),
                          child: Text(
                            item.message,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 15.0,
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(10.0),
                          child: Row(
                            mainAxisAlignment: isMyMessage
                                ? MainAxisAlignment.start
                                : MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              isMyMessage
                                  ? Container()
                                  : Icon(
                                      Icons.check,
                                      color: Colors.blueAccent,
                                      size: 14.0,
                                    ),
                              SizedBox(
                                width: 7.0,
                              ),
                              Text(
                                item.createdAt.toString(),
                                style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 12.0,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        );
      },
    );
  }

  ///When there are no messages in this chat then this widget will be displayed on the screen
  Center _getNoMessagePlaceholderWidget() {
    return Center(
      child: Padding(
        padding: EdgeInsets.only(bottom: 20.0),
        child: Text("No Messages", style: TextStyle(color: Colors.grey)),
      ),
    );
  }

  ///This function will scroll the listview to bottom so that the last message received is visible
  void _scrollToBottom() {
    _scrollController.animateTo(
      _scrollController.position.maxScrollExtent,
      curve: Curves.easeOut,
      duration: const Duration(milliseconds: 300),
    );
  }
}
