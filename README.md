# Agora Voice Call and Chat

This task is for demonstrating the use of Agora SDK for voice calling and using Socket.io for one to one communication

## Getting Started

Agora Demo
For Agora demo we have to add:

- an app id in constants.dart file
- a token is required to run the application, add this in constants.dart file
- "voicechattask", this is the channel used for the voice call.

For Socket chat app:

- We have to update the server ip address by fetching it from the "ifconfig" command.
- The project contains a file "socketbackend.zip", it contains the backend socket server.
- Make sure node is already installed on your system.
- To run the socket server use this command "node index.js"

<!-- This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference. -->
